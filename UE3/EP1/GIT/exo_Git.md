- **Installation + configuration+clonage**

  - **Installation**

    - Debian

      - Installer

      ```bash
      sudo apt install git-all
      ```

      - Version

      ```bash
      git --version
      ```

    - Windows

  - **Configuration**

    - Identité

    ```bash
    git config --global user.name "Jal"
    git config --global user.email "jalil.mestaoui@gmail.com"
    ```

    - Editeur de texte

    ```bash
    git config --global core.editor emacs
    ```

    - Branche par defaut

    ```bash
    git config --global init.defaultBranch main
    ```

    - Verifier configuration

    ```bash
    git config --list
    ```


  - **Cloner depot distant**

    - L 'exemple du TD n'existe plus

    - Cloner un autre depot

    ```bash
    git clone https://github.com/juliendehos/hydra-check.git
    
    ```

    ![](/home/jal/Desktop/1.png)



- **Depot local**

  - **Creer dossier+ ajouter des fichiers + initialiser git +status**

    ```bash
    mkdir exoGit
    cd exoGit
    touch index.html
    touch style.css
    git init
    git status
    ```
    
    ![](/home/jal/Desktop/2.png)
    
  - **add +commit**

    ```bash
    git add *
    git commit -m "debut projet"
    ```

    ![](/home/jal/Desktop/3.png)

  - **Modifier des fichiers**

    ​	![](/home/jal/Desktop/image/4.png)

  - **Ajouter +renemmer+supprimer des fichiers**

    - Ajouter app.java et script.java

    ![](/home/jal/Desktop/5.png)

    - Rennomer app.java en main.java et supprimer script.java

      ![](/home/jal/Desktop/6.png)

    - fffffffffff

      ![](/home/jal/Desktop/7.png)

  - Historique des commits +revenir dans un commit precedent

    - Historique des commits 

    ![](/home/jal/Desktop/8.png)

    - revenir dans un commit precedent (on recupere script.java et app.java)

      ![](/home/jal/Desktop/9.png)

  - Tag

    ![](/home/jal/Desktop/10.png)

  - Suppression des commit





- **Depot distant**

  - Creation depot Git dans gitlab + recuperation  en locale

    ![](/home/jal/Desktop/13.png)

    ![](/home/jal/Desktop/11.png)

  - Ajout +commit + push de qq fichiers

    ![](/home/jal/Desktop/12.png)

    ​	   	![](/home/jal/Desktop/13.png)





​							![](/home/jal/Desktop/14.png)

​                        ![](/home/jal/Desktop/15.png)


​	

- Branche
  - colange depot
  
    ​	![](/home/jal/Desktop/16.png)
  
  - branche b1 + qq commit + fusionner dans main
  
  - envoi b1 sur le serveur
  
  - b2 + qq commit + fusionner dans main
  
  - fusionner main dans b1
  
  - envoi b2 sur serveur + supprimer sur depot locale + supprimersur depot distant
  
- Fork
  - creation depot
  - forker + pull request
  - deux depot un public et un autre privé
  - clonage depot privé , qq commit , push 
  - branch public dans le depot locale +fusionner avec main +pusher sur le depot privé et public
  - commit dans main +fusionner avec la branche public+ push
  - cloner depot public+mettre a jour avec la branch public+ du depot distant privé 
