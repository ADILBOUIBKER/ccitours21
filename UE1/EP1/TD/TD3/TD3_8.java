
/*

	à retenir:

			  --> int table2[] ne sert a rien en java ?? !!!! ...
	          
	          --> l'accès à un tableau statique est 
	          	  plus rapide que l'accès à un tableau dynamique

	          --> System.out.println(Arrays.toString(table3));// pour afficher un tablea	  

*/




import java.lang.Math ;
import java.util.Arrays;

public class TD3_8
{
	public static void main(String [] args)
	{
			

		/*

		Déclarer un tableau de 100 entiers et remplissez-le de valeurs aléatoires comprises entre 1 et 100
		(Math.random() renvoie un nombre sur [0,1[). Affichez le tableau. Comptez le nombre d’éléments.
		*/

		int table1[] = new int[100] ;
		int table2[] = new int[100] ;
		int table3[] = new int[100] ;


        System.out.print("\n\n"); 

		for (int i=0 ; i < 100 ; i++)
		{
			table1[i] = (int) (Math.random() * 100);
			System.out.print("#"+table1[i]+ "#"+"\t"); 
			
		}

		System.out.print("\n\n"); 
		System.out.println( "le nombre d'element du table1 est : " +table1.length); 
		System.out.print("\n\n"); 



		// Filtrer le tableau pour créer un second tableau avec uniquement les nombres pairs

		int index = 0;

		for (int i=0 ; i<100 ; i++)
		{
			

			if (table1[i]%2==0){

				table2[index] = table1[i];

				index++;

				System.out.print("#"+table1[i]+ "#"+"\t"); 
			} 
		}

		System.out.print("\n\n");
		System.out.println( "le nombre d'element du table2 est : " +table2.length); 
		System.out.print("\n\n"); 


		//remplacez tous les nombres impairs par 1, et affichez-le pour contrôler

		for (int i=0 ; i<100 ; i++)
		{
			if (table1[i]%2 != 0) table1[i]=1;
			System.out.print("#"+table1[i]+ "#"+"\t");	

		}
		System.out.print("\n\n"); 	


		// Calculez dans un nouveau tableau (et afficher) les variations successives du tableau des nombres pairs
         
        for (int i=0 ; i<(100-1) ; i++)
        {
        	table3[i]= table2[i+1]-table2[i] ;
        	System.out.print("#"+table3[i]+ "#"+"\t");

        }
        System.out.print("\n\n"); 
   

        
    }
}