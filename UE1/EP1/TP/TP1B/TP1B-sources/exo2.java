import java.util.Arrays;

public class exo2 {

	int[][] tab;

	public exo2(int L, int C) {

		tab = new int[L][C];

		for (int l = 0; l<L; l++) 
			for (int c = 0; c<C; c++) 
				tab[l][c] = (int)(Math.random()*10)+1;
	}

	public void affiche() {

		System.out.println(Arrays.deepToString(tab));
	}

	public boolean Recherche1(int n) {

		boolean TROUVE = false;
		int l = 0;
		while(l<tab.length) {
			int c = 0;
			while (c<tab[0].length) {
				if (tab[l][c] == n)	TROUVE = true;			
				c++;
			}
			l++;
		}
		return TROUVE;
	}

	public int Compte(int n) {
		int nombre = 0;
		for (int[] ligne : tab) { for (int elem : ligne) { if (elem == n) nombre ++;} }
		return nombre;		
	}

	public int[][] Localise(int n) {

		int nombre = Compte(n);
		int[][]pos = new int[nombre][2];
		int k=0;
		int l=0;
		for (int[] ligne : tab) {
			int c = 0;
			for (int elem : ligne) {
				if (elem == n) {
					pos[k][0] = l;
					pos[k][1] = c;
					k++;
				}
				c++;
			}
			l++;
		}
		return pos;
	}

	public static void main(String[] args) {

		exo2 T = new exo2(3,5);

		T.affiche();

		if (T.Recherche1(5))
			System.out.println(" -> 5 est dans le tableau");
		else
			System.out.println(" -> 5 n'est pas dans le tableau");

		if (T.Recherche1(5))
			System.out.println(" -> 5 est présent " + T.Compte(5)+" fois");

		if (T.Recherche1(5)) {
			System.out.println(" -> position(s) du 5 : " +
					Arrays.deepToString(T.Localise(5)));
		}
	}
}


