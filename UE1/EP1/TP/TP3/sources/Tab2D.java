public class Tab2D {

    private int _tab[][];

    public Tab2D(int lig, int col) {

        this._tab = new int[lig][col];
        this.fill(10,90);
    }

    public void set(int lig, int col, int val) {

        if (lig >= 0 && lig < this._tab.length && col >=0 && col < this._tab[0].length) {
            this._tab[lig][col] = val;
        }
        else throw new IndexOutOfBoundsException();
    }

    private void fill(int min, int max) {
        for (int l=0; l<this._tab.length; l++)
            for (int c=0; c<this._tab[l].length; c++) {
                int v = (int)( Math.random() * (max-min) + min );
                this.set(l, c, v);
            }
    }

    public String toString() {
        String res = "";

        for (int l=0; l<this._tab.length; l++) {
            for (int c = 0; c < this._tab[l].length; c++) {
                res = res + this._tab[l][c] + "  ";
            }
            res += "\n";
        }
        return (res);
    }

    public int recherche1(int n)
    {
        return 0;

    }

    public int compte(int n)
    {
        return 0;

    }

    public int localise (int n)
    {
        return 0;

    }


}
