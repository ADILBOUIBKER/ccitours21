public class Sudoku {

	public static void main(String[] args) {

        Grille grille; 
        
        AcquisitionFromFileTXT liregrille = new AcquisitionFromFileTXT();
        grille = liregrille.getGrille("grille.txt");
        /* etat avant resolution */
        grille.display();
		
		SolveurSmart solveur = new SolveurSmart();
		grille = solveur.solve(grille);
		
		/* etat apres resolution */
		grille.display();
	}

}
