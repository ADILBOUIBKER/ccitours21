public class Grille
{
	private int[][] grille;

	public Grille() {
		grille = new int[9][9];
	}

	public void setNumber(int lig, int col, int nb) {
		grille[lig-1][col-1] = nb;
	}

	public int getNumber(int lig, int col) {
		return grille[lig-1][col-1];
	}

	private boolean verifierLigne(int i) {
		int ligne = i;
		boolean correct = true;
		// tous chiffres compris entre 0 et 9
		for (int idx=0; idx<9; idx++) {
			correct = correct && (grille[ligne][idx]>=0) && (grille[ligne][idx]<=9);
		}
		// chaque chiffre >0 n'apparait qu'une fois
		for (int idx=0; idx<8; idx++) {
			for (int ref=idx+1; ref<9; ref++) {
				if (grille[ligne][idx] != 0) { 
					correct = correct && (grille[ligne][idx] != grille[ligne][ref]);    			
				}
			}
		}

		return true;
	}

	private boolean verifierColonne(int i) {
		int colonne = i-1;
		boolean correct = true;
		// tous chiffres compris entre 0 et 9
		for (int idx=0; idx<9; idx++) {
			correct = correct && (grille[idx][colonne]>=0) && (grille[idx][colonne]<=9);
		}
		// chaque chiffre >0 n'apparait qu'une fois
		for (int idx=0; idx<8; idx++) {
			for (int ref=idx+1; ref<9; ref++) {
				if (grille[idx][colonne] != 0) { 
					correct = correct && (grille[idx][colonne] != grille[ref][colonne]);    			
				}
			}
		}

		return true;
	}

	private boolean verifierBloc(int i) {
		int bloc = i-1;
		int[] debc = {0, 3, 6, 0, 3, 6, 0, 3, 6}; // debut d'un bloc (colonne)
		int[] debl = {0, 0, 0, 3, 3, 3, 6, 6, 6}; // debut d'un bloc (ligne)
		int col = debc[bloc];  // 1er elem colonne du bloc
		int lig = debl[bloc];  // 1er elem ligne du bloc

		boolean correct = true;
		// recopie bloc dans une ligne pour simplifier
		int elem[] = new int[9];
		int rang = 0;
		for (int idxc=col; idxc<col+3; idxc++) {
			for (int idxl=lig; idxl<lig+3; idxl++) {    		
				elem[rang++] = grille[idxl][idxc];
			}
		}

		// tous chiffres compris entre 0 et 9
		for (int idx=0; idx<9; idx++) {
			correct = correct && (elem[idx]>=0) && (elem[idx]<=9);
		}
		// chaque chiffre >0 n'apparait qu'une fois
		for (int idx=0; idx<9; idx++) {
			for (int ref=idx+1; ref<9; ref++) {
				if (elem[idx] != 0) { 
					correct = correct && (elem[idx] != elem[ref]);    			
				}
			}
		}

		return true;
	}

	public boolean getEtat() {
		boolean etat = true;

		for (int i=1; i<10; i++) {
			// coherence des chiffres en ligne
			etat = etat && verifierLigne(i);
			if (!etat) break;
			// coherence des chiffres en colonne
			etat = etat && verifierColonne(i);
			if (!etat) break;
			// coherence des chiffres en blocs
			etat = etat && verifierBloc(i);    		
			if (!etat) break;
		}

		return etat;
	}

	public void display() {
		for (int l=0; l<9; l++)
		{
			for (int c=1; c<9; c++)
			{
				System.out.print(grille[l][c]);
				if ( (c+1) % 3 == 0 && c!=8 )
					System.out.print(" | ");
				else
					System.out.print(" ");
			}
			System.out.println();
			if ( (l+1) % 3 == 0 && l!=8 ) System.out.println("------+-------+------");
		}
		System.out.println();
	}

}
