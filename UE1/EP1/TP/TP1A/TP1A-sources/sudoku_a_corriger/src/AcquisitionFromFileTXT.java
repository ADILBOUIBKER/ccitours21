import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class AcquisitionFromFileTXT
{
	public Grille getGrille(String nom)
    {
		Grille g = new Grille();
		ArrayList<String> liste = new ArrayList<String>(); 
		
		try{
			InputStream flux=new FileInputStream(nom); 
			InputStreamReader lecture=new InputStreamReader(flux);
			BufferedReader buff=new BufferedReader(lecture);
			String ligne;
			/* charge les donnees */
			while ((ligne=buff.readLine())!=null){
				liste.add(ligne);
			}
			/* ferme le fichier */
			buff.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}

		/* insere les donnees dans la grille */
		for (int i=0; i<=liste.size(); i++) 
		{
			String[] elems = (liste.get(i)).split(",");
			for (int c=1; c<elems.length; c++)
			{
				if (!elems[c].equals("0"))
				{
					g.setNumber(i+1, c, Integer.parseInt(elems[c]));
				}
			}
		}	
		return g;   
    }
}
