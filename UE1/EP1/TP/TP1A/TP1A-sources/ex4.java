
public class ex4 {

	public static void main(String[] args) {
		double[] tab1 = {2.05,-3.,0.2, 5.75};
		
		int nb = NbValeursPositives(tab1);
		System.out.println("le nombre de valeurs positives est :"+nb);

	}
	
	public static int NbValeursPositives(double[] tab){
		int nbValPosi = 0;
		for (int i = 0; i < tab.length; i++)
			if (tab[i] >=1)
					nbValPosi++;
		return nbValPosi;	
	}
}
