
public class ex1 {

	   /** 
	    * Elevation d'un entier à la puissance entière, positive ou nulle
	    * @param base Entier à élever 
	    * @param exp exposant pour l'élévation
	    * @return base^exp
	    */
		public static long eleve(int base, int exp) 
		{
			long res = (long)base;
			
			for (int i=1; i<=exp; i++)
				res *= base;
			
			return res;
		}
			
	public static void main(String [] args) {
		
		long r1 = eleve(2,4);
		System.out.println("2^4 = " + r1);
		
	}
}
