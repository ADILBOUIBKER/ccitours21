import java.util.Arrays;

public class ex5 {

	public static void inverse(int[] tab){
		for (int i = 0; i < tab.length; i++){
			int tmp = tab[i];
			tab[i] = tab[tab.length - i - 1];
			tab[tab.length - i - 1] = tmp;

		}
	}

	public static void main(String[] args) {
		int [] tab = {2,6,8,12};
		System.out.println("Avant inversion : " + Arrays.toString(tab));
		inverse(tab);
		System.out.println("Après inversion : " + Arrays.toString(tab));
		
	}

}
