

public class Segment
{

	private Point p1 ;
	private Point p2 ;


	public Segment()
	{
		this.p1 = new Point();
		this.p2 = new Point();
	}

	public Segment(Point p1 , Point p2)
	{
		this.p1 = p1 ;
		this.p2 = p2 ;

	}

	public Segment(Segment s) 
	{
		this.p1 = new Point(s.p1);
		this.p2 = new Point(s.p2);
	}

	public Segment(double x1, double y2 , double x2 ,double y2)
	{
		this.p1 = new Point(x1,y1) ;
		this.p2 = new Point(x2, y2); ;

	}

	public double getX1() 
	{
		return this.p1.getX();
	}

	public double getY1() 
	{
		return this.p1.getY();
	}


}